output "instance_ip_address"{
    value = aws_instance.EC2instance.*.public_ip
    description = "Public IP address of EC2 instance"

}