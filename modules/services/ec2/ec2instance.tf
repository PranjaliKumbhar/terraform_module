#Terraform Block
terraform {
  required_version = "v0.14.10"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.36.0"
    }
  }
}

#Simple data module for getting current external IP address
module "myip" {
  source  = "4ops/myip/http"
  version = "1.0.0"
}


#EC2 Instance
resource "aws_instance" "EC2instance" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = "task1-key"
  vpc_security_group_ids = [aws_security_group.SSHRule.id]
  tags = {
    Name = var.tags
  }
}

#KeyPair
resource "aws_key_pair" "owner" {
  key_name   = "task1-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwjCa7xu7niRLcoophQvp/uitbTj5HetmSBauxiTtdUaKPQqOZXoCgsiVLVsujcLOqlrHYmMoCogEqrh0mUuPZluO9Ucv4RNcfjqea3mi5uhYOFSQi4T5LsgenvQFn4u+5yAPAkbpZppi1ae0MIDtlHTSqPSbcqxy+SwzKvxHYyUljfTtw5mkrITCt6ORh9JFGuj8hjcSvaYt24jOLml1/FfY0AOCnXaV9Oh1fKlHiQlcyL2DI1T9QTZKKZiXs908qTgx8LYO5o+qzwODpvUE/wwnQ0blAhr6Z4cj5gPROS37Qcbb5zKEQasJc4PPTlTZ/V0i/ZyXOeyLpQ10A8nL51CAvXVHgC2DVLlRoXnFAhPIyMZjKZZUBCKxT0RNrc2b0BHksVm3G9eA641E9qhlu0SU6I84PgaoRLZ1NiZS8Tq8E3Jj8bUlVlrC6uHtDmY64WF7WbT7ErVn2DEyESh/TfSdqAtBonPmvjkII++mrV4+w21uOjZ02lKY50eZwSrs= Prajwal@DESKTOP-J088C6I"
}

#Security Group
resource "aws_security_group" "SSHRule" {
  name = "EC2_SSH"

  ingress {
    description = "Allow SSH inbound connection"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = formatlist("%s/32",[module.myip.address])
  }
    
  egress {
    description = "All Traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Elastic IP
/*
resource "aws_eip" "EIP" {
  instance = aws_instance.EC2instance.id

}
*/
