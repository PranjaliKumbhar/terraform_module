variable "ssh_port" {
  description = "SSH Port"
  type        = number
  default     = 22
}

variable "instance_type" {
  description = "Instance Type"
  type        = string
}

variable "ami" {
  description = "AMI of Instance"
  type        = string
}

variable "tags" {
  description = "EC2 Instance Tag "
  type = string
}



