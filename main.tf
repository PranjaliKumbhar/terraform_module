# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}


module "ec2instance" {
    source = "E:/Training_LS/Task1_terraform/modules/services/ec2"
    ami = "ami-0742b4e673072066f"
    instance_type = "t2.micro"
    tags = "Task2"
}

